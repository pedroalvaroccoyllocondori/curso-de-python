#conversion de tipo de datos

# a cadena
numero1=1
numero2=2
multiplicacion=numero1*numero2
print('el resultado de multiplicacion es '+str(multiplicacion))


numero=str(235)
decimal=str(1.2)
booleano=str(True)

print(numero,decimal,booleano)
flotante=float('2.3')
flotante1=float(6)

print(flotante1,flotante)

entero=int('5')
entero1=int(2.5)
print(entero1,' y ',entero)

#CASO ESPECIAL los valores vacios se combierten en false
booleano=bool(0)
booleano1=bool('')
booleano2=bool(2)
booleano3=bool(-2)

print(booleano,booleano1,booleano2,booleano3)